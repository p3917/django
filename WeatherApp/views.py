from django.http.response import HttpResponse
from django.shortcuts import render
import json
import requests
from .models import WeatherData
import datetime
import pycountry



# Create your views here.
def Index(request):
    return render(request,'index.html')

def GetCity(request):
    db_city=list(WeatherData.objects.filter(city=request.POST['location']))
    print(len(db_city))

    #if city data not available in db 
    if len(db_city)==0:
        dt='new'
        last_db_data=''
        difference=''
        #call weather data from api
        api='http://api.openweathermap.org/data/2.5/weather?q={}&appid={}'.format(request.POST['location'],'31da291d339f3d99646c2c9ddbce7cae')
        response=requests.get(api)
        json_data=json.loads(response.text)
        cn=json_data['sys']['country']
        de_st = list(pycountry.countries)
        cn_lst=[]
        for de_s in de_st:
            cn_lst.append([de_s.alpha_2,de_s.name])
        for ls in cn_lst:
            if ls[0]==cn:
                country=ls[1]

        min_temp=round(float(json_data['main']['temp_min'])-273.15,1)
        max_temp=round(float(json_data['main']['temp_max'])-273.15,1)
        wind_speed=json_data['wind']['speed']
        temprature=round(float(json_data['main']['temp'])-273.15,1)

        humidity=json_data['main']['humidity']
        city=json_data['name']

        Weather_obj=WeatherData()
        Weather_obj.city=city
        Weather_obj.contry=country
        Weather_obj.humidity=humidity
        Weather_obj.temprature=temprature
        Weather_obj.min_temprature=min_temp
        Weather_obj.max_temprature=max_temp
        Weather_obj.wind_speed=wind_speed
        Weather_obj.last_update=datetime.datetime.now()
        Weather_obj.save()

    #if city data avilable in db
    else:
        dt='already'
        last_db_data=list(WeatherData.objects.filter(city=request.POST['location']).order_by('-id'))[-1]
        temprature=last_db_data.temprature
        max_temp=last_db_data.max_temprature
        min_temp=last_db_data.min_temprature
        humidity=last_db_data.humidity
        wind_speed=last_db_data.wind_speed
        city=last_db_data.city
        country=last_db_data.contry

        #calclutae last update time
        now = datetime.datetime.now()
        now = str(datetime.datetime.strptime(str(now).split('.')[0], "%Y-%m-%d %H:%M:%S"))

        new=last_db_data.last_update
        df=str(datetime.datetime.strptime(str(new).split('.')[0], "%Y-%m-%d %H:%M:%S"))

        a = datetime.datetime(int(now.split('-')[0]),int(now.split('-')[1]),int(now.split('-')[2].split()[0]),int(now.split(' ')[1].split(':')[0]),int(now.split(' ')[1].split(':')[1]),int(now.split(' ')[1].split(':')[2]))
        b = datetime.datetime(int(df.split('-')[0]),int(df.split('-')[1]),int(df.split('-')[2].split()[0]),int(df.split(' ')[1].split(':')[0]),int(df.split(' ')[1].split(':')[1]),int(df.split(' ')[1].split(':')[2]))

        c = str(a-b) 
        if 'days' in str(c):
            difference=c
        else:
            difference=c.split(':')[0]+' Hour '+c.split(':')[1]+' Minutes '+c.split(':')[2]+' Seconds'

    #get data from database for recent searches
    db_data=WeatherData.objects.all().order_by('-id')[:5]

    context={'country':country,'temprature':str(temprature)+'C','max_temp':str(max_temp)+'C','min_temp':str(min_temp)+'C','humidity':humidity,'city':city,'wind_speed':str(wind_speed)+"k/h","list": db_data,'date':difference,'dt':dt}

    return render(request,'index.html',context)

