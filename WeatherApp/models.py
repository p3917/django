from django.db import models
from django.db.models.fields import DateTimeField, FloatField, IntegerField, TextField
# from setting import *

# Create your models here.
class WeatherData(models.Model):
    contry=models.TextField(max_length=20)
    city=models.TextField(max_length=20)
    temprature=models.FloatField(max_length=5)
    min_temprature=models.FloatField(max_length=5)
    max_temprature=models.FloatField(max_length=5)
    wind_speed=models.IntegerField()
    humidity=models.IntegerField()
    last_update=models.DateTimeField()
